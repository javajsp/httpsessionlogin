/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wpsj.model;

import com.wpsj.da.ProductDataAccess;
import com.wpsj.entity.Product;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Setup
 */
public class ProductFinderBean {
    private String keyword;
    private int id;
    
    public void setKeyword(String keyword)
    {this.keyword = keyword;}
    public List<Product> getProducts()
    {
        return new
            ProductDataAccess().getProductsByName(keyword);
    }
    
    public void setKeywordId(int id)
    {this.id = id;}
    public List<Product> getProductsbyId() throws SQLException, ClassNotFoundException
    {
        return new
            ProductDataAccess().getProductsById(id);
    }
}
