/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wpsj.da;

import com.wpsj.entity.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Setup
 */
public class ProductDataAccess {
    private PreparedStatement searchStatement;
    
    private PreparedStatement getSearchStatement() throws SQLException, ClassNotFoundException {
        if(searchStatement == null) {
            Connection connection = new DBConnection().getConnection();
            searchStatement = 
                    connection.prepareStatement("SELECT pro_id, pro_name, pro_desc FROM ProductStore WHERE pro_name like ?");
        }
        return searchStatement;
    }
    
    private PreparedStatement getSearchStatementbyId() throws SQLException, ClassNotFoundException {
        if(searchStatement == null) {
            Connection connection = new DBConnection().getConnection();
            searchStatement = 
                    connection.prepareStatement("SELECT * FROM ProductStore WHERE pro_id = ?");
        }
        return searchStatement;
    }
    
    public void updateProduct(Product product,int id) throws SQLException, ClassNotFoundException {
        Connection connection = new DBConnection().getConnection();
        PreparedStatement pstm = 
                connection.prepareStatement("UPDATE productstore SET pro_name = ?, pro_desc = ? WHERE pro_id = ?");
        pstm.setString(1, product.getName());
        pstm.setString(2, product.getDesc());
        pstm.setInt(3, id);
        pstm.executeUpdate();
    }
    
    public List<Product> getProductsByName(String name) {
        try {
            PreparedStatement statement = getSearchStatement();
            statement.setString(1, "%"+name+"%");
            ResultSet rs = statement.executeQuery();
            List<Product> products = new LinkedList<Product>();
            while(rs.next())
            {
                products.add(new Product(rs.getInt("pro_id"),
                    rs.getString("pro_name"), rs.getString("pro_desc")));
            }
            return products;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public List<Product> getProductsById(int id) throws SQLException, ClassNotFoundException {
            PreparedStatement statement = getSearchStatementbyId();
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            List<Product> products = new LinkedList<Product>();
            while(rs.next())
            {
                products.add(new Product(rs.getInt("pro_id"),
                    rs.getString("pro_name"), rs.getString("pro_desc")));
            }
            //Product product = new Product(rs.getInt("pro_id"), rs.getString("pro_name"), rs.getString("pro_desc"));
            return products;
    }
}