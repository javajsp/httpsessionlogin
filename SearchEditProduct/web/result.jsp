<%-- 
    Document   : result
    Created on : Apr 25, 2019, 3:18:27 PM
    Author     : Setup
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>List Product</title>
        <style>
            td {
                padding-right: 20px;
            }
        </style>
    </head>
    <body>
        <h1>List Product!</h1>
        <a href="search.jsp">Search</a>
        <jsp:useBean class="com.wpsj.model.ProductFinderBean" id="finder" scope="request"/>
        <table>
            <tr>
                <td>Id</td>
                <td>Name</td>
                <td>Description</td>
                <td>Action</td>
            </tr>
            <c:forEach items="${finder.products}" var="product">
                <tr>
                    <td><c:out value="${product.id}"/></td>
                    <td><c:out value="${product.name}"/></td>
                    <td><c:out value="${product.desc}"/></td>
                    <td><button><a href="ProductEdit?key=${keyword}&Id=${product.id}">Edit</a></button></td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
