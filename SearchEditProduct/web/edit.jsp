<%-- 
    Document   : edit
    Created on : May 19, 2019, 6:22:15 PM
    Author     : Setup
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>List Product!</h1>
        <a href="search.jsp">Search</a>
        <jsp:useBean class="com.wpsj.model.ProductFinderBean" id="finder" scope="request"/>
            <c:forEach items="${finder.productsbyId}" var="product">
                <form action="ProductEdit?Id=${product.id}&key=${keyword}" method="POST">
                    <span class="sp">Name: </span> <input name="name" value="${product.name}"/><br>
                    <span>Desc: </span><input name="desc" value="${product.desc}"/><br>
                    <button type="submit">Submit</button>
                </form>
            </c:forEach>
    </body>
</html>
